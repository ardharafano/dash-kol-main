<!-- <div id="event"
    class="font-Montserrat lg:flex justify-center items-center bg-[url('../images/event/bg.png')] bg-cover bg-no-repeat bg-center w-full py-16 px-4">
    <div class="lg:max-w-[1280px] lg:w-full lg:m-auto" id="fade-up-card" data-hs="fade up">

        <section class="splide head-event" aria-label="Splide Basic HTML Example">

            <div class="splide__track">
                <ul class="splide__list">
                    <li class="splide__slide p-4">
                        <div class="lg:max-w-[970px] lg:w-full lg:m-auto" id="fade-up-card" data-hs="fade up">
                            <div class="flex flex-wrap">
                                <div class="sm:w-6/12">
                                    <div class="text-white">
                                        <h4 class="text-[#A8A8A8] font-bold text-xl mb-3">Event</h4>
                                        <h1 class="font-bold text-3xl mb-5 leading-tight">Kolaborasi Event
                                        </h1>
                                        <p class="text-lg mt-5">Lorem Ipsum is simply dummy text of the printing and
                                            typesetting
                                            industry. Lorem Ipsum has been the industry's standard dummy text ever since
                                            the
                                            1500s, when
                                            an unknown printer took a galley of type and scrambled it to make a type
                                            specimen book</p>
                                    </div>
                                </div>

                                <div class="sm:w-6/12 mt-3">

                                    <section class="splide foto-event-1" aria-label="Splide Basic HTML Example">
                                        <div class="splide__arrows hidden">
                                            <button
                                                class="splide__pagination__page bg-transparent shadow splide__arrow splide__arrow--prev">
                                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                                    stroke-width="1.5" stroke="currentColor"
                                                    class="w-6 h-6 text-transparent">
                                                    <path stroke-linecap="round" stroke-linejoin="round"
                                                        d="M4.5 12h15m0 0l-6.75-6.75M19.5 12l-6.75 6.75" />
                                                </svg>
                                            </button>
                                            <button
                                                class="splide__pagination__page bg-transparent shadow splide__arrow splide__arrow--next">
                                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                                    stroke-width="1.5" stroke="currentColor"
                                                    class="w-6 h-6 text-transparent">
                                                    <path stroke-linecap="round" stroke-linejoin="round"
                                                        d="M4.5 12h15m0 0l-6.75-6.75M19.5 12l-6.75 6.75" />
                                                </svg>
                                            </button>
                                        </div>
                                        <div class="splide__track">
                                            <ul class="splide__list">
                                                <li class="splide__slide p-4">
                                                    <img src="assets/images/event/foto.png" alt="img"
                                                        class="max-w-[531px] w-full h-auto m-auto block rounded-2xl object-cover">
                                                </li>
                                                <li class="splide__slide p-4">
                                                    <img src="assets/images/event/foto.png" alt="img"
                                                        class="max-w-[531px] w-full h-auto m-auto block rounded-2xl object-cover">
                                                </li>
                                                <li class="splide__slide p-4">
                                                    <img src="assets/images/event/foto.png" alt="img"
                                                        class="max-w-[531px] w-full h-auto m-auto block rounded-2xl object-cover">
                                                </li>
                                                <li class="splide__slide p-4">
                                                    <img src="assets/images/event/foto.png" alt="img"
                                                        class="max-w-[531px] w-full h-auto m-auto block rounded-2xl object-cover">
                                                </li>
                                            </ul>
                                        </div>
                                    </section>

                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="splide__slide p-4">
                        <div class="lg:max-w-[970px] lg:w-full lg:m-auto" id="fade-up-card" data-hs="fade up">
                            <div class="flex flex-wrap">
                                <div class="sm:w-6/12">
                                    <div class="text-white">
                                        <h4 class="text-[#A8A8A8] font-bold text-xl mb-3">Event</h4>
                                        <h1 class="font-bold text-3xl mb-5 leading-tight">Kolaborasi Event
                                        </h1>
                                        <p class="text-lg mt-5">Lorem Ipsum is simply dummy text of the printing and
                                            typesetting
                                            industry. Lorem Ipsum has been the industry's standard dummy text ever since
                                            the
                                            1500s, when
                                            an unknown printer took a galley of type and scrambled it to make a type
                                            specimen book</p>
                                    </div>
                                </div>

                                <div class="sm:w-6/12 mt-3">

                                    <section class="splide foto-event-2" aria-label="Splide Basic HTML Example">
                                        <div class="splide__arrows hidden">
                                            <button
                                                class="splide__pagination__page bg-transparent shadow splide__arrow splide__arrow--prev">
                                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                                    stroke-width="1.5" stroke="currentColor"
                                                    class="w-6 h-6 text-transparent">
                                                    <path stroke-linecap="round" stroke-linejoin="round"
                                                        d="M4.5 12h15m0 0l-6.75-6.75M19.5 12l-6.75 6.75" />
                                                </svg>
                                            </button>
                                            <button
                                                class="splide__pagination__page bg-transparent shadow splide__arrow splide__arrow--next">
                                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                                    stroke-width="1.5" stroke="currentColor"
                                                    class="w-6 h-6 text-transparent">
                                                    <path stroke-linecap="round" stroke-linejoin="round"
                                                        d="M4.5 12h15m0 0l-6.75-6.75M19.5 12l-6.75 6.75" />
                                                </svg>
                                            </button>
                                        </div>
                                        <div class="splide__track">
                                            <ul class="splide__list">
                                                <li class="splide__slide p-4">
                                                    <img src="assets/images/event/foto.png" alt="img"
                                                        class="max-w-[531px] w-full h-auto m-auto block rounded-2xl object-cover">
                                                </li>
                                                <li class="splide__slide p-4">
                                                    <img src="assets/images/event/foto.png" alt="img"
                                                        class="max-w-[531px] w-full h-auto m-auto block rounded-2xl object-cover">
                                                </li>
                                                <li class="splide__slide p-4">
                                                    <img src="assets/images/event/foto.png" alt="img"
                                                        class="max-w-[531px] w-full h-auto m-auto block rounded-2xl object-cover">
                                                </li>
                                                <li class="splide__slide p-4">
                                                    <img src="assets/images/event/foto.png" alt="img"
                                                        class="max-w-[531px] w-full h-auto m-auto block rounded-2xl object-cover">
                                                </li>
                                            </ul>
                                        </div>
                                    </section>

                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="splide__slide p-4">
                        <div class="lg:max-w-[970px] lg:w-full lg:m-auto" id="fade-up-card" data-hs="fade up">
                            <div class="flex flex-wrap">
                                <div class="sm:w-6/12">
                                    <div class="text-white">
                                        <h4 class="text-[#A8A8A8] font-bold text-xl mb-3">Event</h4>
                                        <h1 class="font-bold text-3xl mb-5 leading-tight">Kolaborasi Event
                                        </h1>
                                        <p class="text-lg mt-5">Lorem Ipsum is simply dummy text of the printing and
                                            typesetting
                                            industry. Lorem Ipsum has been the industry's standard dummy text ever since
                                            the
                                            1500s, when
                                            an unknown printer took a galley of type and scrambled it to make a type
                                            specimen book</p>
                                    </div>
                                </div>

                                <div class="sm:w-6/12 mt-3">

                                    <section class="splide foto-event-3" aria-label="Splide Basic HTML Example">
                                        <div class="splide__arrows hidden">
                                            <button
                                                class="splide__pagination__page bg-transparent shadow splide__arrow splide__arrow--prev">
                                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                                    stroke-width="1.5" stroke="currentColor"
                                                    class="w-6 h-6 text-transparent">
                                                    <path stroke-linecap="round" stroke-linejoin="round"
                                                        d="M4.5 12h15m0 0l-6.75-6.75M19.5 12l-6.75 6.75" />
                                                </svg>
                                            </button>
                                            <button
                                                class="splide__pagination__page bg-transparent shadow splide__arrow splide__arrow--next">
                                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                                    stroke-width="1.5" stroke="currentColor"
                                                    class="w-6 h-6 text-transparent">
                                                    <path stroke-linecap="round" stroke-linejoin="round"
                                                        d="M4.5 12h15m0 0l-6.75-6.75M19.5 12l-6.75 6.75" />
                                                </svg>
                                            </button>
                                        </div>
                                        <div class="splide__track">
                                            <ul class="splide__list">
                                                <li class="splide__slide p-4">
                                                    <img src="assets/images/event/foto.png" alt="img"
                                                        class="max-w-[531px] w-full h-auto m-auto block rounded-2xl object-cover">
                                                </li>
                                                <li class="splide__slide p-4">
                                                    <img src="assets/images/event/foto.png" alt="img"
                                                        class="max-w-[531px] w-full h-auto m-auto block rounded-2xl object-cover">
                                                </li>
                                                <li class="splide__slide p-4">
                                                    <img src="assets/images/event/foto.png" alt="img"
                                                        class="max-w-[531px] w-full h-auto m-auto block rounded-2xl object-cover">
                                                </li>
                                                <li class="splide__slide p-4">
                                                    <img src="assets/images/event/foto.png" alt="img"
                                                        class="max-w-[531px] w-full h-auto m-auto block rounded-2xl object-cover">
                                                </li>
                                            </ul>
                                        </div>
                                    </section>

                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </section>
    </div>
</div> -->

<div id="event"
    class="font-Montserrat lg:flex justify-center items-center bg-[url('../images/event/bg.png')] bg-cover bg-no-repeat bg-center w-full py-16 px-4">

    <div class="lg:max-w-[970px] lg:w-full lg:m-auto" id="fade-up-card" data-hs="fade up">
        <div class="flex flex-wrap">
            <div class="sm:w-6/12">
                <div class="text-white">
                    <h4 class="text-[#A8A8A8] font-bold text-xl mb-3">Event</h4>
                    <h1 class="font-bold text-3xl mb-5 leading-tight">Kolaborasi Event
                    </h1>
                    <p class="text-lg mt-5">Nexus Creator Hub merupakan produk dan unit layanan terbaru dari PT Integra
                        Archipelago Media
                        yang mencoba fokus pada dunia pemasaran dan bisnis, sembari tetap menjaga dan mengembangkan
                        ide-ide dan kreativitas konten.</p>
                </div>
            </div>

            <div class="sm:w-6/12 mt-3">

                <section class="splide foto-event-1" aria-label="Splide Basic HTML Example">
                    <div class="splide__arrows hidden">
                        <button
                            class="splide__pagination__page bg-transparent shadow splide__arrow splide__arrow--prev">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                                stroke="currentColor" class="w-6 h-6 text-transparent">
                                <path stroke-linecap="round" stroke-linejoin="round"
                                    d="M4.5 12h15m0 0l-6.75-6.75M19.5 12l-6.75 6.75" />
                            </svg>
                        </button>
                        <button
                            class="splide__pagination__page bg-transparent shadow splide__arrow splide__arrow--next">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                                stroke="currentColor" class="w-6 h-6 text-transparent">
                                <path stroke-linecap="round" stroke-linejoin="round"
                                    d="M4.5 12h15m0 0l-6.75-6.75M19.5 12l-6.75 6.75" />
                            </svg>
                        </button>
                    </div>
                    <div class="splide__track">
                        <ul class="splide__list">
                            <li class="splide__slide p-4">
                                <img src="assets/images/event/Water Adventure 2021.JPG" alt="img"
                                    class="max-w-[531px] w-full h-auto m-auto block rounded-2xl object-cover">
                            </li>
                            <li class="splide__slide p-4">
                                <img src="assets/images/event/Desa Brilian 2022.JPG" alt="img"
                                    class="max-w-[531px] w-full h-auto m-auto block rounded-2xl object-cover">
                            </li>
                            <li class="splide__slide p-4">
                                <img src="assets/images/event/Local Media Summit 2022.JPG" alt="img"
                                    class="max-w-[531px] w-full h-auto m-auto block rounded-2xl object-cover">
                            </li>
                            <li class="splide__slide p-4">
                                <img src="assets/images/event/Podcast Cipta Karya 2021.JPG" alt="img"
                                    class="max-w-[531px] w-full h-auto m-auto block rounded-2xl object-cover">
                            </li>
                            <li class="splide__slide p-4">
                                <img src="assets/images/event/Transmate Fest 2021.jpg" alt="img"
                                    class="max-w-[531px] w-full h-auto m-auto block rounded-2xl object-cover">
                            </li>
                        </ul>
                    </div>
                </section>

            </div>
        </div>
    </div>

</div>


<script>
// var splide = new Splide('.splide.head-event', {
//     perPage: 1,
//     rewind: true,
//     autoplay: true,
//     pagination: false,
//     drag: false,
//     breakpoints: {
//         991.98: {
//             perPage: 1,
//             gap: "1rem",
//             // destroy: true,
//         },
//         768: {
//             perPage: 1,
//             gap: "1rem",
//             // destroy: true,
//         },
//     },
// });

// splide.mount();

var splide = new Splide('.splide.foto-event-1', {
    perPage: 1,
    rewind: true,
    autoplay: true,
    breakpoints: {
        991.98: {
            perPage: 1,
            gap: "1rem",
            // destroy: true,
        },
        768: {
            perPage: 1,
            gap: "1rem",
            // destroy: true,
        },
    },
});

splide.mount();

// var splide = new Splide('.splide.foto-event-2', {
//     perPage: 1,
//     rewind: true,
//     autoplay: true,
//     breakpoints: {
//         991.98: {
//             perPage: 1,
//             gap: "1rem",
//             // destroy: true,
//         },
//         768: {
//             perPage: 1,
//             gap: "1rem",
//             // destroy: true,
//         },
//     },
// });

// splide.mount();

// var splide = new Splide('.splide.foto-event-3', {
//     perPage: 1,
//     rewind: true,
//     autoplay: true,
//     breakpoints: {
//         991.98: {
//             perPage: 1,
//             gap: "1rem",
//             // destroy: true,
//         },
//         768: {
//             perPage: 1,
//             gap: "1rem",
//             // destroy: true,
//         },
//     },
// });

// splide.mount();
</script>