<div id="tentang-kami" class="font-Montserrat py-16 px-4">
    <div class="lg:max-w-[970px] lg:w-full lg:m-auto" id="fade-up-card" data-hs="fade up">
        <h2 class="text-[#2B3990] font-bold text-xl mb-3 text-start">Tentang Kami</h3>
            <div class="flex flex-wrap">

                <p class="mb-5 leading-loose"><b>Nexus -</b> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                    has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a
                    galley of type and scrambled it to make a type specimen book. It has survived not only five
                    centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was
                    popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and
                    more recently with desktop publishing software like Aldus PageMaker including versions of Lorem
                    Ipsum.</p>
                <p class="leading-loose">Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
                    standard dummy text ever since the 1500s, when an unknown printer took a galley of type and
                    scrambled it to make a type specimen book. It has survived not only five centuries, but also the
                    leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s
                    with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop
                    publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
            </div>
    </div>
</div>