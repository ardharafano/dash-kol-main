<div id="tentang" class="font-Montserrat py-16 px-4">
    <div class="lg:max-w-[970px] lg:w-full lg:m-auto" id="fade-up-card" data-hs="fade up">
        <div class="flex flex-wrap">

            <div class="sm:w-6/12">
                <h4 class="text-[#A8A8A8] font-bold text-2xl">Tentang</h4>
                <h2 class="text-[#2B3990] font-bold text-3xl mb-3">Apa itu Nexus Creator Hub?</h3>
                    <div class="lg:pr-20 mb-5">
                        <p>Inilah platform yang menjembatani antara brand dengan para creator atau influencer lokal yang
                            menjangkau audiens sesungguhnya. Nexus Creator Hub adalah wadah bagi para creator lokal
                            untuk
                            berkembang dan berhubungan langsung dengan brand-brand besar, sebaliknya juga menjadi
                            saluran
                            efektif bagi brand untuk menjangkau langsung pasarnya.</p>
                        <!-- <ul class="list-disc ml-5">
                            <li>Lokal</li>
                            <li>Segmented</li>
                        </ul> -->
                    </div>
                    <div class="flex items-center gap-3 mb-5">
                        <img src="assets/images/tentang/icon-1.svg" alt="img" class="float-left w-[62px] h-[62px]">
                        <p><b>Meningkatkan brand awareness</b></p>
                    </div>

                    <div class="flex items-center gap-3 mb-5">
                        <img src="assets/images/tentang/icon-2.svg" alt="img" class="float-left w-[62px] h-[62px]">
                        <p><b>Kepercayaan konsumen meningkat</b></p>
                    </div>

                    <div class="flex items-center gap-3 mb-5">
                        <img src="assets/images/tentang/icon-3.svg" alt="img" class="float-left w-[62px] h-[62px]">
                        <p><b>Meningkatkan penjualan</b></p>
                    </div>
            </div>

            <div class="sm:w-6/12">
                <img src="assets/images/tentang/tentang.png" alt="img"
                    class="max-w-[450px] w-full h-auto m-auto block rounded-2xl object-cover"
                    style="filter: drop-shadow(0px 18px 30px rgba(0, 0, 0, 0.25));">
            </div>

        </div>
    </div>
</div>