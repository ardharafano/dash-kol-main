<div id="karir" class="font-Montserrat py-16 px-4">
    <div class="lg:max-w-[970px] lg:w-full lg:m-auto" id="fade-up-card" data-hs="fade up">
        <h2 class="text-[#2B3990] font-bold text-xl mb-3 text-start">Karir</h3>
            <div class="flex flex-wrap">

                <h3 class="font-bold mb-3">WE ARE RECRUITING ARE YOU THE ONE?</h3>
                <p class="mb-5">Arkadia Digital Media is an independent and integrated digital media group managing
                    several online
                    media with various topics such as local and international politics, current affairs, entertainment,
                    technology, sports, automotive, business, health and lifestyle, available on various platforms and
                    delivered through many channels.
                    Right now, we offer some good career opportunities for the following positions:</p>

                <div class="bg-[#DBDADA] p-10 rounded-2xl mb-10">
                    <p>PLACEMENT IN YOGYAKARTA</p>
                    <h3 class="font-bold mb-5">VIDEOGRAPHER / VIDEO EDITOR</h3>
                    <p>Requirements:</p>
                    <ul class="list-disc ml-10">
                        <li>Male/Female, maximum 30 years old</li>
                        <li>Min. Diploma Degree from any major</li>
                        <li>Have work experience in the same posisition at least 2 years</li>
                        <li>Having good knowledge in using Adobe Illustration software, Adobe Photoshop Excellent in
                            Final Cut Pro or Adobe premiere Pro Good Team playe</li>
                    </ul>
                </div>

                <div>
                    <p class="font-bold mb-5">Silahkan kirim Aplikasi dan CV anda ke:</p>
                    <p class="font-bold">HUMAN RESOURCES DEVELOPMENT</p>
                    <p>PT. Arkadia Media Nusantara</p>
                    <p>Jln Mega Kuningan Timur Blok C6 Kav.9</p>
                    <p>Kawasan Mega Kuningan,Jakarta 12950, Indonesia</p>
                    <p>Email: recruitment@suara.com</p>
                    <p>Masukkan [judul lowongan] posisi di subject email</p>
                </div>

            </div>
    </div>
</div>