<div id="campaign" class="font-Montserrat py-16 px-4">
    <div class="lg:max-w-[970px] lg:w-full lg:m-auto" id="fade-up-card" data-hs="fade up">
        <div class="flex flex-wrap">
            <div class="sm:w-full text-center py-28 rounded-2xl px-5" style="background: linear-gradient(82.35deg, #ED1C24 22.92%, #FF3D00 108.59%);
                        box-shadow: 0px 8px 35px rgba(0, 0, 0, 0.25);">
                <h1 class="text-white text-3xl font-semibold mb-12 lg:max-w-[500px] lg:w-full lg:mx-auto">Silakan
                    langsung mulai promosikan brand Anda!</h1>

                <div class="flex flex-wrap justify-center gap-5">
                    <a href="#"
                        class="bg-white text-[#ED1C24] py-6 px-20 m rounded-[55px] font-bold w-full max-w-[350px] hover:bg-slate-100">Buat
                        Campaign</a>

                    <a href="dashboard.php?dashboard=influencer-register"
                        class="bg-white text-[#ED1C24] py-6 px-20 rounded-[55px] font-bold w-full max-w-[350px] hover:bg-slate-100">Sebagai Influencer</a>
                </div>

            </div>

        </div>
    </div>
</div>

<div id="bg">

    <img src="assets/images/campaign/bg-right.svg" alt="img"
        class="absolute right-0 sm:top-[1280px] sm:w-[250px] top-[1500px] w-[220px] -z-20">
    <img src="assets/images/campaign/bg-left.svg" alt="img"
        class="absolute left-0 sm:top-[1500px] sm:w-[250px] top-[1700px] w-[220px] -z-20">

</div>