<div id="collabolators" class="font-Montserrat py-16 px-4">
    <div class="lg:max-w-[970px] lg:w-full lg:m-auto" id="fade-up-card" data-hs="fade up">
        <h2 class="text-[#2B3990] font-bold text-xl mb-3 text-center">Collabolators</h3>
            <div class="flex flex-wrap justify-center items-center">

                <a href="https://bri.co.id/" target="_blank">
                    <img src="assets/images/network/bri.png" alt="image" width="120" height="45" class="m-3" />
                </a>
                <a href="https://www.pu.go.id/" target="_blank">
                    <img src="assets/images/network/pupr.png" alt="image" width="321" height="45" class="m-3" />
                </a>
                <a href="https://dephub.go.id/" target="_blank">
                    <img src="assets/images/network/menhub-laut.png" alt="image" width="399" height="60" class="m-3" />
                </a>
                <a href="https://dephub.go.id/" target="_blank">
                    <img src="assets/images/network/kemenhub.png" alt="image" width="350" height="55" class="m-3" />
                </a>

            </div>
    </div>
</div>

<!-- <div id="network" class="font-Montserrat pt-0 pb-16 px-4">
    <div class="lg:max-w-[970px] lg:w-full lg:m-auto" id="fade-up-card" data-hs="fade up">
        <h2 class="text-[#2B3990] font-bold text-xl mb-3 text-center">Network</h3>
            <div class="flex flex-wrap justify-center items-center">

                <a href="https://www.youtube.com/channel/UCIPBADJECZeHH_eYCWu-jnQ?app=desktop" target="_blank">
                    <img src="assets/images/network/hamburger.png" alt="image" width="72" height="72" class="m-3" />
                </a>
                <a href="https://www.babad.id/" target="_blank">
                    <img src="assets/images/network/babad.png" alt="image" width="105" height="35" class="m-3" />
                </a>
                <a href="https://bincangperempuan.com/" target="_blank">
                    <img src="assets/images/network/bincang-perempuan.png" alt="image" width="70" height="70" class="m-3" />
                </a>
                <a href="https://digitalmama.id/" target="_blank">
                    <img src="assets/images/network/digitalmama.png" alt="image" width="84" height="40" class="m-3"/>
                </a>
                <a href="https://indotnesia.suara.com/" target="_blank">
                    <img src="assets/images/network/indotnesia.png" alt="image" width="100" height="66" class="m-3"/>
                </a>

            </div>
    </div>
</div> -->