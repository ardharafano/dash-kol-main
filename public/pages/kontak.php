<div id="kontak" class="font-Montserrat py-16 px-4">
    <div class="lg:max-w-[970px] lg:w-full lg:m-auto" id="fade-up-card" data-hs="fade up">
        <h2 class="text-[#2B3990] font-bold text-xl mb-3 text-start">Kontak</h3>
            <div class="flex flex-wrap">

                <div class="mb-5">
                    <p class="font-bold">PT. Arkadia Media Nusantara</p>
                    <p>Jln Mega Kuningan Timur Blok C6 Kav.9</p>
                    <p>Kawasan Mega Kuningan,Jakarta 12950, Indonesia</p>
                    <p>Tel: 021 - 50101239</p>
                    <p>Email: redaksi@suara.com</p>
                    <p>Iklan: sales@suara.com Info Kirim Tulisan: yoursay@suara.com</p>
                </div>

                <div style="max-width:100%;overflow:hidden;color:red;width:970px;height:500px;">
                    <div id="embed-map-display" style="height:100%; width:100%;max-width:100%;"><iframe
                            style="height:100%;width:100%;border:0;" frameborder="0"
                            src="https://www.google.com/maps/embed/v1/place?q=Arkadia+Digital+Media,+Jalan+Mega+Kuningan+Timur,+RT.8/RW.2,+Kuningan,+East+Kuningan,+South+Jakarta+City,+Jakarta,+Indonesia&key=AIzaSyBFw0Qbyq9zTFTd-tUY6dZWTgaQzuU17R8"></iframe>
                    </div><a class="embed-ded-maphtml" href="https://www.bootstrapskins.com/themes"
                        id="authorize-maps-data">premium bootstrap themes</a>
                    <style>
                    #embed-map-display img {
                        max-width: none !important;
                        background: none !important;
                        font-size: inherit;
                        font-weight: inherit;
                    }
                    </style>
                </div>

                <form>
                    <h1 class="text-center my-5 font-bold text-xl">Hubungi Kami</h1>
                    <div class="flex flex-wrap justify-center">
                        <div class="sm:w-6/12 w-full px-4 mb-5">
                            <input type="text" placeholder="Nama" class="input input-bordered w-full"  required/>
                        </div>
                        <div class="sm:w-6/12 w-full px-4 mb-5">
                            <input type="email" placeholder="Email" class="input input-bordered w-full" required/>
                        </div>
                        <div class="sm:w-6/12 w-full px-4 mb-5">
                            <input type="number" placeholder="No. Handphone" class="input input-bordered w-full" required/>
                        </div>
                        <div class="sm:w-6/12 w-full px-4 mb-5">
                            <textarea class="textarea textarea-bordered textarea-lg w-full" placeholder="Pesan" required></textarea>
                        </div>
                        <div class="mb-5 text-center">
                            <button type="submit"
                                class="button bg-[#2B3990] text-white font-bold w-full mx-auto py-4 px-10 rounded-2xl">KIRIM</button>
                        </div>
                    </div>
                </form>

            </div>
    </div>
</div>