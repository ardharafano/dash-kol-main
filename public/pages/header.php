<div id="header"
    class="font-Montserrat flex justify-center items-center bg-[url('../images/header/bg2.png')] bg-cover bg-no-repeat bg-center w-full min-h-screen px-4">
    <div class="lg:max-w-[970px] lg:w-full lg:m-auto">
        <div class="flex flex-wrap">
            <div class="lg:w-full xl:max-w-[550px] xl:ml-[500px] max-xl:m-auto max-xl:w-full max-xl:text-center">
                <div class="text-white max-md:text-center">
                    <!-- <h4 class="font-bold text-lg mb-5">Lorem Ipsum is simply dummy</h4> -->
                    <h1 class="font-bold text-[35px] mb-5 leading-tight">Maksimalkan jangkauan brand Anda lewat jaringan
                        creator &amp; influencer lokal terbaik!
                    </h1>
                    <p class="text-lg my-5">Jangkauan luas dan lebih terarah. Campaign atau konten bisa
                        sekreatif mungkin. Harga
                        pun bersahabat. Tunggu apa lagi?</p>
                </div>
                <a href="dashboard.php?dashboard=login">
                    <div class="font-bold text-white w-52 rounded-[25px] mt-3 text-center text-lg p-3 max-xl:m-auto"
                        style="background: linear-gradient(90deg, #ED1C24 4.89%, #FF3D00 100%);">
                        Buat Campaign
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>