<div id="talent" class="font-Montserrat py-16 px-4" id="fade-up-card" data-hs="fade up">
    <div class="text-center">
        <!-- <h4 class="text-[#A8A8A8] font-bold text-2xl">Creator</h4> -->
        <h2 class="text-[#2B3990] font-bold text-3xl mb-3">Creators</h3>
    </div>
    <div class="lg:max-w-[970px] lg:w-full lg:m-auto">

        <!-- <div class="flex flex-wrap justify-center">
            <div class="sm:w-3/12 px-3 pb-3">
                <img src="assets/images/talent/KOL_foto_aco.jpg" alt="img"
                    class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
            </div>

            <div class="sm:w-3/12 px-3 pb-3">
                <img src="assets/images/talent/KOL_foto_adhia.jpg" alt="img"
                    class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
            </div>

            <div class="sm:w-3/12 px-3 pb-3">
                <img src="assets/images/talent/KOL_foto_david.jpg" alt="img"
                    class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
            </div>

            <div class="sm:w-3/12 px-3 pb-3">
                <img src="assets/images/talent/KOL_foto_tenri.jpg" alt="img"
                    class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
            </div>

            <div class="sm:w-3/12 px-3 pb-3">
                <img src="assets/images/talent/KOL_foto_gamal.jpg" alt="img"
                    class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
            </div>
        </div> -->

        <section class="splide talent" aria-label="Splide Basic HTML Example">
            <ul class="splide__pagination"></ul>
            <div class="splide__track">
                <ul class="splide__list">
                    <li class="splide__slide p-4">
                        <img src="assets/images/talent/KOL_foto_aco.jpg" alt="img"
                            class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                    </li>
                    <li class="splide__slide p-4">
                        <img src="assets/images/talent/KOL_foto_adhia.jpg" alt="img"
                            class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                    </li>
                    <li class="splide__slide p-4">
                        <img src="assets/images/talent/KOL_foto_david.jpg" alt="img"
                            class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                    </li>
                    <li class="splide__slide p-4">
                        <img src="assets/images/talent/KOL_foto_tenri.jpg" alt="img"
                            class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                    </li>
                    <li class="splide__slide p-4">
                        <img src="assets/images/talent/KOL_foto_gamal.jpg" alt="img"
                            class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                    </li>
                </ul>
            </div>
        </section>

    </div>
</div>

<script>
var splide = new Splide('.splide.talent', {
    perPage: 4,
    rewind: true,
    autoplay: true,
    breakpoints: {
        991.98: {
            perPage: 2,
            gap: "1rem",
            // destroy: true,
        },
        768: {
            perPage: 1,
            gap: "1rem",
            // destroy: true,
        },
    },
});

splide.mount();
</script>