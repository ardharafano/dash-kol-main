<div id="forgot_password" class="font-Montserrat flex justify-center items-center min-h-screen">
    <div class="container">
        <div class="w-full m-auto">
            <div class="flex flex-wrap">

                <div class="lg:w-6/12 w-full px-5">
                    <form action="?dashboard=login" method="post">
                        <div class="w-full max-w-[450px] mx-auto md:mt-52 mt-0">

                            <div>
                                <img src="assets/images/navbar/logo.svg" class="w-[266px] h-[68px] mx-auto my-20 block"
                                    alt="img" width="266" height="68">
                            </div>
                            <h1 class="text-center mb-5 font-bold text-lg">Lupa Password</h1>
                            <div class="mb-5">
                                <input type="text" placeholder="Username or Email"
                                    class="input input-bordered w-full max-w-md border-[#1E1E1E] text-center rounded-2xl" />
                            </div>
                            <div class="mb-5 text-center">
                                <button type="submit"
                                    class="button bg-[#FB2A08] text-white font-bold max-w-[450px] w-full mx-auto py-4 rounded-2xl">RESET
                                    PASSWORD</button>
                            </div>
                        </div>
                    </form>
                    <div class="text-center">
                        <div class="text-sm text-[#1E1E1E]">Belum Memiliki Akun?</div>
                    </div>
                    <div class="mb-20 text-center">
                        <a class="text-[#1E1E1E] font-bold" href="?dashboard=register">Daftar Akun</a>
                    </div>
                    <div class="flex justify-center items-center max-w-[160px] w-full mx-auto mb-5">
                        <span class="block text-xs mr-2">Created by</span>
                        <a href="#">
                            <img src="assets/images/dashboard/arkadiame.svg" alt="img">
                        </a>
                    </div>
                </div>

                <div class="lg:w-6/12 w-full">
                    <img src="assets/images/dashboard/login3.png" alt="img" class="form-bg">
                </div>

            </div>
        </div>
    </div>
</div>