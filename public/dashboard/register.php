<div id="register" class="font-Montserrat flex justify-center items-center min-h-screen">
    <div class="container">
        <div class="w-full m-auto">
            <div class="flex flex-wrap">

                <div class="lg:w-6/12 w-full px-5">
                    <form action="?dashboard=login" method="post">
                        <div class="w-full max-w-[450px] mx-auto xl:mt-20 mt-0">

                            <div>
                                <img src="assets/images/navbar/logo.svg" class="w-[266px] h-[68px] mx-auto my-20 block"
                                    alt="img" width="266" height="68">
                            </div>
                            <h1 class="text-center mb-5 font-bold text-lg">REGISTER</h1>
                            <div class="mb-5">
                                <input type="text" placeholder="Email"
                                    class="input input-bordered w-full max-w-md border-[#1E1E1E] text-center rounded-2xl" />
                            </div>
                            <div class="mb-5">
                                <input type="password" placeholder="Password"
                                    class="input input-bordered w-full max-w-md border-[#1E1E1E] text-center rounded-2xl" />
                            </div>
                            <div class="mb-5">
                                <input type="text" placeholder="Nama Brand"
                                    class="input input-bordered w-full max-w-md border-[#1E1E1E] text-center rounded-2xl" />
                            </div>
                            <div class="mb-5">
                                <input type="text" placeholder="Username"
                                    class="input input-bordered w-full max-w-md border-[#1E1E1E] text-center rounded-2xl" />
                            </div>
                            <div class="mb-5">
                                <input type="text" placeholder="No.HP"
                                    class="input input-bordered w-full max-w-md border-[#1E1E1E] text-center rounded-2xl" />
                            </div>
                            <div class="mb-5">
                                <select
                                    class="select border-[#1E1E1E] w-full max-w-md text-center text-[#8D8D8D] rounded-2xl font-normal text-[16px]">
                                    <option disabled selected>Interest</option>
                                    <option>Han Solo</option>
                                    <option>Greedo</option>
                                </select>
                            </div>
                            <div class="flex flex-nowrap mb-5">
                                <div class="lg:w-6/12 w-full px-0.5">
                                    <select
                                        class="select select-bordered border-[#1E1E1E] w-full max-w-md text-center text-[#8D8D8D] rounded-2xl font-normal text-[16px]">
                                        <option disabled selected>Provinsi</option>
                                        <option>Han Solo</option>
                                        <option>Greedo</option>
                                    </select>
                                </div>
                                <div class="lg:w-6/12 w-full px-0.5">
                                    <select
                                        class="select select-bordered border-[#1E1E1E] w-full max-w-md text-center text-[#8D8D8D] rounded-2xl font-normal text-[16px]">
                                        <option disabled selected>Kota</option>
                                        <option>Han Solo</option>
                                        <option>Greedo</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-control">
                                <label class="flex justify-center my-3 gap-1">
                                    <input type="checkbox" class="checkbox checkbox-primary" />
                                    <span class="label-text">Saya setuju dengan <b><a href="#">Syarat &
                                                Ketentuan</a></b></span>
                                </label>
                            </div>

                            <div class="my-3 text-center">
                                <button type="submit"
                                    class="button bg-[#FB2A08] text-white font-bold max-w-[450px] w-full mx-auto py-4 rounded-2xl">DAFTAR</button>
                            </div>

                        </div>
                    </form>

                    <div class="text-center">
                        <div class="text-sm text-[#1E1E1E]">Sudah Memiliki Akun?</div>
                    </div>
                    <div class="mb-20 text-center">
                        <a class="text-[#1E1E1E] font-bold" href="?dashboard=login">Masuk</a>
                    </div>
                    <div class="flex justify-center items-center max-w-[160px] w-full mx-auto mb-5">
                        <span class="block text-xs mr-2">Created by</span>
                        <a href="#">
                            <img src="assets/images/dashboard/arkadiame.svg" alt="img">
                        </a>
                    </div>
                </div>

                <div class="lg:w-6/12 w-full">
                    <!-- <img src="assets/images/dashboard/login2.png" alt="img"
                        class="lg:min-w-[800px] sm:min-w-[auto] sm:max-w-[500px] w-full h-auto block mx-auto object-cover"> -->
                    <!-- <img src="assets/images/dashboard/login2.png" alt="img"
                        class="xl:absolute xl:top-0 xl:right-[-50px] mx-auto"> -->
                    <img src="assets/images/dashboard/login3.png" alt="img" class="form-bg">
                </div>

            </div>
        </div>
    </div>
</div>