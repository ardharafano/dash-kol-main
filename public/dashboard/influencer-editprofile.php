<!-- begin::Navbar -->
<div class="navbar bg-[#F1F5F9] px-4 fixed z-50">
    <div class="flex-1 gap-4">
        <button aria-label="button"
            class="btn btn-square btn-ghost bg-slate-300 hover:bg-slate-400 btn-navbar lg:hidden">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="#0f172a">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h7" />
            </svg>
        </button>
        <!-- <a href="?dashboard=dash">
                <img src="assets/images/navbar/logo.svg" alt="img">
            </a> -->
    </div>
    <div class="flex-none md:gap-2">
        <!-- <div class="form-control order-1 hidden md:block">
                <input type="text" placeholder="Search" class="input input-bordered" />
            </div> -->
        <div class="dropdown dropdown-end order-3 md:order-2">
            <label tabindex="0" class="btn btn-ghost btn-circle avatar relative">
                <span class="flex h-3 w-3 absolute top-1 right-1">
                    <span
                        class="animate-ping absolute inline-flex h-full w-full rounded-full bg-red-400 opacity-75"></span>
                    <span class="relative inline-flex rounded-full h-3 w-3 bg-red-500"></span>
                </span>
                <i class="fa fa-bell text-slate-600 text-xl"></i>
            </label>
            <div tabindex="0"
                class="mt-3 shadow menu menu-compact dropdown-content bg-base-100 rounded-md w-80 sm:w-96 overflow-hidden">
                <div class="text-center text-slate-800 border-b py-3 text-md">Notification</div>
                <div class="max-h-72 overflow-y-auto">
                    <a href="#" class="hover:bg-slate-100 hover:text-slate-500 flex px-4 py-2 gap-4">
                        <div class="avatar">
                            <div class="w-12 h-12 rounded-full">
                                <img src="assets/images/dashboard/avatar-2.jpg" alt="img">
                            </div>
                        </div>
                        <div>
                            <div class="mb-2">
                                <span class="text-primary">Jeremy Rakestraw</span> accepted your invitation to
                                join
                                the team
                            </div>
                            <div class="text-slate-400 text-xs">2 MIN AGO</div>
                        </div>
                    </a>
                    <a href="#" class="hover:bg-slate-100 hover:text-slate-500 flex px-4 py-2 gap-4">
                        <div class="avatar">
                            <div class="w-12 h-12 rounded-full">
                                <img src="assets/images/dashboard/avatar-2.jpg" alt="img">
                            </div>
                        </div>
                        <div>
                            <div class="mb-2">
                                <span class="text-primary">Jeremy Rakestraw</span> accepted your invitation to
                                join
                                the team
                            </div>
                            <div class="text-slate-400 text-xs">2 MIN AGO</div>
                        </div>
                    </a>
                    <a href="#" class="hover:bg-slate-100 hover:text-slate-500 flex px-4 py-2 gap-4">
                        <div class="avatar">
                            <div class="w-12 h-12 rounded-full">
                                <img src="assets/images/dashboard/avatar-2.jpg" alt="img">
                            </div>
                        </div>
                        <div>
                            <div class="mb-2">
                                <span class="text-primary">Jeremy Rakestraw</span> accepted your invitation to
                                join
                                the team
                            </div>
                            <div class="text-slate-400 text-xs">2 MIN AGO</div>
                        </div>
                    </a>
                    <a href="#" class="hover:bg-slate-100 hover:text-slate-500 flex px-4 py-2 gap-4">
                        <div class="avatar">
                            <div class="w-12 h-12 rounded-full">
                                <img src="assets/images/dashboard/avatar-2.jpg" alt="img">
                            </div>
                        </div>
                        <div>
                            <div class="mb-2">
                                <span class="text-primary">Jeremy Rakestraw</span> accepted your invitation to
                                join
                                the team
                            </div>
                            <div class="text-slate-400 text-xs">2 MIN AGO</div>
                        </div>
                    </a>
                </div>
                <a href="#" class="text-center bg-primary block py-3 font-bold text-white hover:bg-blue-800">View
                    All
                    Notifications</a>
            </div>
        </div>
        <div class="dropdown dropdown-end order-2 md:order-3">
            <label tabindex="0" class="btn btn-ghost btn-circle avatar">
                <div class="w-10 rounded-full">
                    <img src="assets/images/dashboard/avatar-1.jpg" alt="img">
                </div>
            </label>
            <ul tabindex="0" class="mt-3 shadow menu menu-compact dropdown-content bg-base-100 rounded-md w-64">
                <li class="bg-primary text-white block px-4 py-2">
                    <h6 class="text-lg font-bold p-0 hover:bg-transparent cursor-default">John Doe</h6>
                    <h6 class="p-0 hover:bg-transparent cursor-default">Available</h6>
                </li>
                <li>
                    <a class="py-3 text-slate-600">
                        <i class="fa fa-user"></i>
                        Account
                    </a>
                </li>
                <li>
                    <a class="py-3 text-slate-600">
                        <i class="fa fa-cog"></i>
                        Setting
                    </a>
                </li>
                <li>
                    <a href="index.php" class="py-3 text-slate-600">
                        <i class="fa fa-power-off"></i>
                        Logout
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- end::Navbar -->

<div class="drawer drawer-mobile">
    <input id="my-drawer-2" type="checkbox" class="drawer-toggle" aria-label="input" />
    <div class="drawer-content items-center justify-center relative bg-slate-100">
        <ul class="menu lg:hidden p-4 text-white fixed left-0 right-0 top-[65px] w-full items-start z-[10] transition duration-300 -translate-y-full navbar-menu"
            style="background: linear-gradient(27.34deg, #2A3C90 40.67%, #B81B1B 119.37%);">
            <!-- <div class="p-4 w-full">
                    <h6 class="font-bold">DASHBOARD</h6>
                </div> -->
            <li class="w-full">
                <a href="?dashboard=dash">
                    <img src="assets/images/navbar/logo-light.svg" alt="img" class="mb-3">
                </a>
            </li>
            <li class="w-full">
                <a href="?dashboard=influencer" class="py-3 rounded-sm hover:bg-[#4A1481]">
                    <i class="fa fa-user-circle"></i>
                    Influencer
                </a>
            </li>
            <li class="w-full">
                <a href="?dashboard=influencer-finder" class="py-3 rounded-sm hover:bg-[#4A1481]">
                    <i class="fa fa-search"></i>
                    Influencer Finder
                </a>
            </li>
            <li class="w-full">
                <a href="?dashboard=influencer-profile" class="py-3 rounded-sm hover:bg-[#4A1481] bg-[#4A1481]">
                    <i class="fa fa-user-circle"></i>
                    Influencer Profile
                </a>
            </li>
        </ul>

        <main class="px-6 py-8 mt-16">
            <!-- begin::Header -->
            <header class="mb-8">
                <h1 class="text-xl md:text-3xl font-medium">Influencer Edit Profile</h1>
                <div class="divider my-0"></div>
                <div class="text-sm breadcrumbs my-0">
                    <ul>
                        <li><a>Dashboard</a></li>
                        <li>Influencer Profile</li>
                        <li>Edit</li>
                    </ul>
                </div>
            </header>
            <!-- end::Header -->

            <!-- begin::Content -->
            <form action="" method="POST">
                <small class="block italic mb-5">* Wajib diisi</small>
                <div class="text-sm mb-6">
                    <span class="block mb-2">Email *</span>
                    <input type="email" class="input input-bordered w-full" />
                </div>

                <div class="text-sm mb-6">
                    <span class="block mb-2">Nama Lengkap *</span>
                    <input type="text" class="input input-bordered w-full" />
                </div>

                <div class="text-sm mb-6">
                    <span class="block mb-2">Tanggal Lahir *</span>
                    <input type="date" class="input input-bordered w-full" />
                </div>

                <div class="text-sm mb-6">
                    <span class="block mb-2">Domisili *</span>
                    <input type="text" class="input input-bordered w-full" />
                </div>

                <div class="text-sm mb-6">
                    <span class="block mb-2">Minat *</span>
                    <div class="form-control">
                        <label class="flex gap-3 my-2">
                            <input type="radio" name="radio-10" class="radio checked:bg-[#43239E]" />
                            <span class="label-text">Kuliner</span>
                        </label>
                    </div>
                    <div class="form-control">
                        <label class="flex gap-3 my-2">
                            <input type="radio" name="radio-10" class="radio checked:bg-[#43239E]" />
                            <span class="label-text">Sport</span>
                        </label>
                    </div>
                    <div class="form-control">
                        <label class="flex gap-3 my-2">
                            <input type="radio" name="radio-10" class="radio checked:bg-[#43239E]" />
                            <span class="label-text">Beauty</span>
                        </label>
                    </div>
                    <div class="form-control">
                        <label class="flex gap-3 my-2">
                            <input type="radio" name="radio-10" class="radio checked:bg-[#43239E]" />
                            <span class="label-text">Komedi</span>
                        </label>
                    </div>
                    <div class="form-control">
                        <label class="flex gap-3 my-2">
                            <input type="radio" name="radio-10" class="radio checked:bg-[#43239E]" />
                            <span class="label-text">Yang Lain <input type="text"
                                    class="input input-bordered w-full mt-2" /></span>
                        </label>
                    </div>
                </div>


                <div class="text-sm mb-6">
                    <span class="block mb-2">Akun Instagram *</span>
                    <input type="text" class="input input-bordered w-full" />
                </div>

                <div class="text-sm mb-6">
                    <span class="block mb-2">Akun Tiktok</span>
                    <input type="text" class="input input-bordered w-full" />
                </div>

                <div class="text-sm mb-6">
                    <span class="block mb-2">Akun Snack Video</span>
                    <input type="text" class="input input-bordered w-full" />
                </div>

                <div class="text-sm mb-6">
                    <span class="block mb-2">Akun Youtube</span>
                    <input type="text" class="input input-bordered w-full" />
                </div>


                <div class="mb-10">
                    <span class="block mb-2">Mau jadi content creator apa? *</span>
                    <small>Boleh diisi lebih dari satu</small>
                    <div class="form-control">
                        <label class="flex gap-3 my-2">
                            <input type="checkbox" class="checkbox" />
                            <span class="label-text">Instagram</span>
                        </label>
                    </div>
                    <div class="form-control">
                        <label class="flex gap-3 my-2">
                            <input type="checkbox" class="checkbox" />
                            <span class="label-text">TikTok</span>
                        </label>
                    </div>
                    <div class="form-control">
                        <label class="flex gap-3 my-2">
                            <input type="checkbox" class="checkbox" />
                            <span class="label-text">Snack Video</span>
                        </label>
                    </div>
                    <div class="form-control">
                        <label class="flex gap-3 my-2">
                            <input type="checkbox" class="checkbox" />
                            <span class="label-text">Youtube</span>
                        </label>
                    </div>
                    <div class="form-control">
                        <label class="flex gap-3 my-2">
                            <input type="checkbox" class="checkbox" />
                            <span class="label-text">Yang Lain <input type="text"
                                    class="input input-bordered w-full mt-2" /></span>
                        </label>
                    </div>
                </div>

            </form>

            <div class="flex gap-5 justify-center">
                <a href="?dashboard=influencer-profile">
                    <button class="bg-[#333458] rounded-lg text-white py-3 px-10 font-semibold">Simpan
                    </button>
                </a>

                <a href="?dashboard=influencer-profile">
                    <button class="bg-[#FB2A08] rounded-lg text-white py-3 px-10 font-semibold">Kembali
                    </button>
                </a>
            </div>
            <!-- end::Content -->
        </main>

        <!-- begin::Footer -->
        <div id="footer">
            <div class="mb-3 lg:pr-10 px-4">
                <p class="lg:text-right text-center text-sm">© 2023 nexuscreatorhub.com - a subsidiary of Arkadia
                    Digital Media</p>
            </div>
        </div>
        <!-- end::Footer -->

    </div>
    <div class="drawer-side lg:z-50">
        <label for="my-drawer-2" class="drawer-overlay" aria-label="input"></label>
        <ul class="menu p-4 w-80 text-white"
            style="background: linear-gradient(27.34deg, #2A3C90 40.67%, #B81B1B 119.37%);">
            <!-- <div class="p-4 w-full">
                    <h6 class="font-bold">DASHBOARD</h6>
                </div> -->
            <li>
                <a href="?dashboard=dash">
                    <img src="assets/images/navbar/logo-light.svg" alt="img" class="mb-3">
                </a>
            </li>
            <li>
                <a href="?dashboard=influencer" class="py-3 rounded-sm hover:bg-[#4A1481]">
                    <i class="fa fa-user-circle"></i>
                    Influencer
                </a>
            </li>
            <li>
                <a href="?dashboard=influencer-finder" class="py-3 rounded-sm hover:bg-[#4A1481]">
                    <i class="fa fa-search"></i>
                    Influencer Finder
                </a>
            </li>
            <li>
                <a href="?dashboard=influencer-profile" class="py-3 rounded-sm hover:bg-[#4A1481] bg-[#4A1481]">
                    <i class="fa fa-user-circle"></i>
                    Influencer Profile
                </a>
            </li>
        </ul>

    </div>
</div>