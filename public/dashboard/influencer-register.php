<div id="register" class="font-Montserrat flex justify-center items-center min-h-screen">
    <div class="container">
        <div class="w-full m-auto">
            <div class="flex flex-wrap">

                <div class="w-full px-5">
                    <form action="?dashboard=influencer-editprofile" method="post">
                        <div class="w-full max-w-[450px] mx-auto xl:mt-20 mt-0">

                            <div>
                                <img src="assets/images/navbar/logo.svg" class="w-[266px] h-[68px] mx-auto my-20 block"
                                    alt="img" width="266" height="68">
                            </div>
                            <h1 class="text-center mb-5 font-bold text-lg">DAFTAR SEBAGAI INFLUENCER</h1>
                            <div class="mb-5">
                                <input type="text" placeholder="Email"
                                    class="input input-bordered w-full max-w-md border-[#1E1E1E] text-center rounded-2xl" />
                            </div>
                            <div class="mb-5">
                                <input type="password" placeholder="Password"
                                    class="input input-bordered w-full max-w-md border-[#1E1E1E] text-center rounded-2xl" />
                            </div>

                            <div class="form-control">
                                <label class="flex justify-center my-3 gap-1">
                                    <input type="checkbox" class="checkbox checkbox-primary" />
                                    <span class="label-text">Saya setuju dengan <b><a href="#">Syarat &
                                                Ketentuan</a></b></span>
                                </label>
                            </div>

                            <div class="my-3 text-center">
                                <button type="submit"
                                    class="button bg-[#FB2A08] text-white font-bold max-w-[450px] w-full mx-auto py-4 rounded-2xl">DAFTAR</button>
                            </div>

                        </div>
                    </form>

                    <div class="text-center">
                        <div class="text-sm text-[#1E1E1E]">Sudah Memiliki Akun?</div>
                    </div>
                    <div class="mb-20 text-center">
                        <a class="text-[#1E1E1E] font-bold" href="?dashboard=influencer-login">Masuk</a>
                    </div>
                    <div class="flex justify-center items-center max-w-[160px] w-full mx-auto mb-5">
                        <span class="block text-xs mr-2">Created by</span>
                        <a href="#">
                            <img src="assets/images/dashboard/arkadiame.svg" alt="img">
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>