<!DOCTYPE html>
<html lang="en" class="scroll-smooth" data-theme="light">

<head>
    <title>Suara - KOL</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Suara KOL">
    <meta name="keywords" content="Suara, Suara KOL, Influencer, Key Opinion Leader">

    <meta property="og:title" content="suara.com" />
    <meta property="og:description" content="Key Opinion Leader" />
    <meta property="og:url" content="https://www.suara.com/" />
    <meta property="og:image" content="assets/images/navbar/logo.svg" />

    <link rel="Shortcut icon" href="assets/images/navbar/logo.svg">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
        rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@splidejs/splide@3/dist/js/splide.min.js"></script>
    <script src="assets/js/main.js?<?= time() ?>"></script>

    <!-- tailwind -->
    <link rel="stylesheet" href="assets/css/tailwind.css?<?= time() ?>">
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/flowbite/1.6.3/flowbite.min.js"></script> -->
    <!-- end tailwind -->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css"
        integrity="sha512-q3eWabyZPc1XTCmF+8/LuE1ozpg5xxn7iO89yfSOd5/oKvyqLngoNGsx8jq92Y8eXJ/IRxQbEC+FGSYxtk2oiw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
</head>

<body class="font-Montserrat">

    <?php
            if(isset($_GET['dashboard'])){ 
                $url = 'index.php?';
                include("dashboard/".$_GET['dashboard'].".php");
            }else{
                $url = 'index.php?';
                include("dashboard/influencer.php");
            }
        ?>

</body>

</html>

<script>
document.addEventListener("DOMContentLoaded", function() {
    setTimeout(() => {
        window.scrollTo(0, 0);
    }, 20000);
    document.querySelector(".btn-navbar").addEventListener("click", function() {
        document
            .querySelector(".navbar-menu")
            .classList.toggle("-translate-y-full");
    });
});
</script>

<script>
$(document).ready(function() {
    jQuery('img').each(function() {
        jQuery(this).attr('src', jQuery(this).attr('src') + '?' + (new Date()).getTime());
    });
});
</script>