<div id="footer">
    <div class="font-Montserrat">
        <div class="lg:max-w-[970px] lg:w-full lg:m-auto">
            <h1 class="font-bold text-xl text-center py-5">Ikuti Kami</h1>
            <div class="flex justify-center items-center">
                <!-- <a href="#">
                    <img src="assets/images/footer/fb.svg"
                        class="my-0 mx-3 border-[1px] border-[#dadada] rounded-xl w-12 h-12 p-2" alt="img">
                </a>

                <a href="#">
                    <img src="assets/images/footer/twt.svg"
                        class="my-0 mx-3 border-[1px] border-[#dadada] rounded-xl w-12 h-12 p-2"" alt=" img">
                </a> -->

                <a href="#">
                    <img src="assets/images/footer/yt.svg"
                        class="my-0 mx-3 border-[1px] border-[#dadada] rounded-xl w-12 h-12 p-2"" alt=" img">
                </a>

                <a href="#">
                    <img src="assets/images/footer/ig.svg"
                        class="my-0 mx-3 border-[1px] border-[#dadada] rounded-xl w-12 h-12 p-2"" alt=" img">
                </a>
                <a href="#">
                    <img src="assets/images/footer/tiktok.svg"
                        class="my-0 mx-3 border-[1px] border-[#dadada] rounded-xl w-12 h-12 p-2"" alt=" img">
                </a>
            </div>
            <p class="text-center mt-3">Dapatkan informasi terkini dan terbaru yang dikirimkan langsung ke Inbox
                anda
            </p>

            <!-- <div class="mt-3">
                <p class="text-center">PT. Integra Archipelago Media</p>
                <p class="text-center">Jl. Mega Kuningan Timur Blok C6 Kav.9</p>
                <p class="text-center">Kawasan Mega Kuningan,</p>
                <p class="text-center">Jakarta 12950, Indonesia</p>
                <div class="text-center">Email:
                    <a href="mailto:hello@nexuscreatorhub.com">
                        <input type="submit" value="hello@nexuscreatorhub.com" class="cursor-pointer" width="32"
                            height="32">
                    </a>
                </div>
            </div> -->

        </div>
    </div>

    <div class="font-Montserrat bg-[#2B3990] p-4 my-5">
        <div class="lg:max-w-[970px] lg:w-full lg:m-auto">
            <div class="flex flex-wrap gap-3 justify-center items-center">
                <!-- <a href="#" class="text-white border-r-[1px] border-[#dadada] pr-4">
                    Redaksi
                </a> -->
                <a href="?page=kontak" class="text-white border-r-[1px] border-[#dadada] pr-4">
                    Kontak
                </a>
                <a href="?page=tentang-kami" class="text-white border-r-[1px] border-[#dadada] pr-4">
                    Tentang Kami
                </a>
                <a href="?page=karir" class="text-white border-r-[1px] border-[#dadada] pr-4">
                    Karir
                </a>
                <!-- <a href="#" class="text-white border-r-[1px] border-[#dadada] pr-4">
                    Pedoman Media Siber
                </a> -->
                <a href="?page=sitemap" class="text-white pr-4">
                    Site Map
                </a>
            </div>
        </div>
    </div>

    <div class="font-Montserrat mb-3">
        <div class="lg:max-w-[970px] lg:w-full lg:m-auto px-4">
            <p class="text-center text-sm">© 2023 nexuscreatorhub.com - a subsidiary of Arkadia Digital Media
            </p>
        </div>
    </div>
</div>