<nav class="font-Montserrat border-gray-200 px-4 sm:px-4 py-4 rounded fixed w-full z-20 top-0 left-0 transition-all">
    <div class="lg:max-w-[970px] lg:w-full lg:m-auto">

        <div class="container flex flex-wrap items-center justify-between mx-auto">
            <a href="index.php" class="flex items-center">
                <img src="assets/images/navbar/logo.svg" class="mr-3 w-[140px] h-[35px] logo" alt="img"
                    style="filter:brightness(0%) contrast(200%) saturate(0%) grayscale(100%) invert(100%) ">
            </a>
            <div class="flex items-start md:items-center md:order-2">

                <a href="dashboard.php?dashboard=login">
                    <button type="button"
                        class="text-black font-medium rounded-lg text-sm px-5 py-[4px] text-center mr-3 bg-white border border-[#d9d9d9] hover:bg-[#d9d9d9]">Login</button>
                </a>

                <button id="dropdownNavbarLink" data-dropdown-toggle="dropdownNavbar" aria-label="button"
                    class="flex items-center justify-between w-full pt-1.5 pl-3 pr-4 font-medium text-white button-search rounded md:border-0 md:p-0 md:w-auto">
                    <svg class="w-5 h-5" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20"
                        xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                            d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
                            clip-rule="evenodd"></path>
                    </svg>
                </button>

                <div id="dropdownNavbar"
                    class="z-10 hidden font-normal bg-white divide-y divide-gray-100 rounded-lg shadow w-44 dark:bg-gray-700 dark:divide-gray-600">
                    <ul class="" aria-labelledby="dropdownLargeButton">
                        <li>
                            <input type="text" id="search-navbar"
                                class="block w-full p-2 text-sm text-gray-900 border border-gray-300 rounded-lg  focus:ring-blue-500 focus:border-blue-500"
                                placeholder="Search...">
                        </li>
                    </ul>
                </div>

                <button data-collapse-toggle="navbar-sticky" type="button"
                    class="inline-flex items-center p-[5px] text-sm  text-white button-hamburger rounded-lg md:hidden focus:outline-none focus:ring-2 focus:ring-gray-200 "
                    aria-controls="navbar-sticky" aria-expanded="false">
                    <span class="sr-only">Open main menu</span>
                    <svg class="w-6 h-6" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20"
                        xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                            d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z"
                            clip-rule="evenodd"></path>
                    </svg>
                </button>
            </div>
            <div class="items-center justify-between hidden w-full md:flex md:w-auto md:order-1" id="navbar-sticky">
                <ul
                    class="konten-nav bg-black bg-opacity-[0.9] sm:!bg-transparent font-semibold mt-3 text-white flex flex-col  rounded-lg  md:flex-row md:space-x-8 xl:space-x-12 md:mt-0 md:text-sm md:font-bold md:border-0 ">
                    <li>
                        <a href="index.php#header"
                            class="block py-2 pl-3 pr-4 text-nav rounded hover:bg-gray-400 md:hover:bg-transparent md:hover:text-blue-700 md:p-0"
                            aria-current="page">Home</a>
                    </li>
                    <li>
                        <a href="index.php#tentang"
                            class="block py-2 pl-3 pr-4 text-nav rounded hover:bg-gray-400 md:hover:bg-transparent md:hover:text-blue-700 md:p-0 ">Tentang</a>
                    </li>
                    <li>
                        <a href="index.php#talent"
                            class="block py-2 pl-3 pr-4 text-nav rounded hover:bg-gray-400 md:hover:bg-transparent md:hover:text-blue-700 md:p-0">Creators</a>
                    </li>
                    <li>
                        <a href="index.php#event"
                            class="block py-2 pl-3 pr-4 text-nav rounded hover:bg-gray-400 md:hover:bg-transparent md:hover:text-blue-700 md:p-0">Event</a>
                    </li>
                    <li>
                        <a href="index.php#footer"
                            class="block py-2 pl-3 pr-4 text-nav rounded hover:bg-gray-400 md:hover:bg-transparent md:hover:text-blue-700 md:p-0">Kontak</a>
                    </li>
                    <!-- <li>
                        <a href="#footer"
                            class="block py-2 pl-3 pr-4 text-nav rounded hover:bg-gray-300 md:hover:bg-transparent md:hover:text-blue-700 md:p-0">Login</a>
                    </li> -->
                </ul>
            </div>
        </div>
    </div>
</nav>

<script>
$(window).scroll(function() {
    if ($(window).scrollTop() >= 100) {
        // scroll
        $('nav').css('background', 'white');
        $('.button-search').css('color', 'black');
        $('.konten-nav').css('background-color', 'transparent');
        $('.text-nav').css('color', '#494652');
        $('.button-hamburger').css('color', 'black');
        $('.logo').css('filter', 'none');
    } else {
        // top
        $('nav').css('background', 'transparent');
        $('.button-search').css('color', 'white');
        $('.konten-nav').css('background-color', 'rgba(0, 0, 0, 0.9)');
        $('.text-nav').css('color', 'white');
        $('.button-hamburger').css('color', 'white');
        $('.logo').css('filter', 'brightness(0%) contrast(200%) saturate(0%) grayscale(100%) invert(100%) ');
    }
});
</script>